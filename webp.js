const glob = require("glob");
const colors = require("colors");
const imagemin = require("imagemin");
const webp = require("imagemin-webp");

const folder = "./resources/assets/images/";

glob("**/*.+(png)", {
    cwd: folder
}, pngToWebp);

glob("**/*.+(jpeg|jpg)", {
    cwd: folder
}, jpgToWebp);

function pngToWebp (er, files) {
    console.log("=====================================");
    console.log("Converting " + "jpg".blue + " files to webp");

    convertToWebp(files, {
        lossless: true
    }, file => {
        console.log("Done converting ".green + "png ".blue + file.path.green);
    });
}

function jpgToWebp (er, files, quality = 100) {
    console.log("=====================================");
    console.log("Converting " + "jpg".blue + " files to webp");

    let notConvertedJpgs = [];

    convertToWebp(files, {
        quality: quality
    }, file => {
        if (file.path.toLowerCase().endsWith(".jpg") || file.path.toLowerCase().endsWith(".jpeg")) {
            console.log(("Failed to convert '" + file.path + "' will retry").red);

            // Make sure the filepath is the same as initially before we throw it in the array
            let filePath = file.path.replace(folder.substring(2), '');
            notConvertedJpgs.push(filePath);
        } else {
            console.log("Done converting ".green + "jpg ".blue + file.path.green);
        }
    }, () => {
        // Done converting batch of files. Time to try the all the leftover jpegs at a lower quality.
        if (notConvertedJpgs.length === 0) {
            console.log("There are no jpeg files to retry, moving on".green);
            console.log("=====================================");
            return;
        }

        console.log("=====================================");
        console.log("There are " + notConvertedJpgs.length + " files left to retry");
        console.log("Retrying at quality " + (quality - 5).toString().blue);
        jpgToWebp(null, notConvertedJpgs, quality - 5);
    });
}

function convertToWebp(files, options, perFileCallback = false, doneCallback = false) {
    let convertedFiles = 0;
    const filesToConvert = files.length;

    console.log("Found " + filesToConvert.toString().blue + " files to convert to webp");
    console.log("=====================================");

    files.forEach(function(file) {
        console.log("File to convert: " + file);

        let folders = file.split("/");
        folders.pop();
        let fullOutputFolder = folder + folders.join("/");

        imagemin([folder + file], fullOutputFolder, {
            pwd: '',
            plugins: [webp(options)]
        }).then(files => {
            convertedFiles++;

            if (perFileCallback !== false) {
                perFileCallback(files[0]);
            }

            if (convertedFiles === filesToConvert && doneCallback !== false) {
                doneCallback();
            }
        });
    });
    console.log("=====================================");
}
