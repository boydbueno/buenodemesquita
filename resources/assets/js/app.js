
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Vue = require('vue');

require('intersection-observer');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('bb-header', require('Components/Header.vue'));
Vue.component('bb-projects', require('Components/Projects.vue'));
Vue.component('bb-footer', require('Components/Footer.vue'));

const app = new Vue({
    el: '#app',

    computed: {
        projectsGroupedByYear() {
            let years = [];

            let yearIndex = 0;
            let yearMap = {};

            this.projects.forEach(project => {
                if (!(project.year in yearMap)) {
                    yearMap[project.year] = yearIndex;
                    years[yearIndex] = {
                        year: project.year,
                        projects: []
                    }
                    yearIndex++;
                }

                return years[yearMap[project.year]].projects.push(project);
            });

            years.sort((a, b) => {
               if (a.year > b.year) {
                   return -1;
               }
               if (a.year < b.year) {
                   return 1;
               }
               return 0;
            });

            return years;
        }
    },

    data: {
        projects: [
            {
                year: 2017,
                title: "Flappy Fish",
                description: `
                    <p>
                        A Flappy Bird clone with a small twist.
                    </p>
                    <p>
                        As soon as you hit the water you'll be pushed up. 'Flap' by pressing spacebar or by clicking to set your velocity. 
                        Use this to reduce your speed when you're going too fast or to gain a bit speed in case you're falling short.
                    </p>
                `,
                duration: false,
                teamSize: false,
                labels: [
                    {
                        label: "PixiJS",
                        category: "engine",
                    },
                    {
                        label: "Programming",
                        category: "role"
                    },
                    {
                        label: "Design",
                        category: "role"
                    },
                    {
                        label: "Browser",
                        category: "platform"
                    }
                ],
                accomplishments: false,
                readMore: false,
                download: {
                    platform: "browser",
                    link: "https://boydbueno.github.io/flappy-fish/"
                },
                source: {
                    platform: "Github",
                    link: "https://github.com/Boydbueno/flappy-fish"
                },
                image: {
                    "placeholder": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wgARCAA6AGYDASIAAhEBAxEB/8QAGgAAAgMBAQAAAAAAAAAAAAAAAAUCBAYBA//EABkBAAIDAQAAAAAAAAAAAAAAAAIDAAQFAf/aAAwDAQACEAMQAAAB5JnxuUqmzOcWDMMVg05VesGZaQsGZImGRlX9PBWXKPXKKaXtvNaOQ5UwglziSgclz5qidvCUTZtFU1c6z2qIbaKo9VqNcErRVCG0VST1Kpl3vU0wLcx3TAFmTTBDmY6gEsyaYIcyaYkypqhLf//EACIQAAEDBQADAAMAAAAAAAAAAAACAwQBERITFCAiIxAwM//aAAgBAQABBQLjbOJsVDTfibOJs4mzibFwm0o4mzibOJs4myTEQ3HKX8nf5+EhFXG1R2Ki4zIppPVzxxMZi1IrWNI8elHUJq8uMznSMxnzx605mMKtRUJ/DifXwcT6+ExqimLlxdfW5cuXFVLly5ckV+GRkLV65GRkZGRkZGRkSFfDUs1LFNLrTUs1LNSzUsUyupqWalmpZqWOsrU1+3//xAAkEQABAwIGAgMAAAAAAAAAAAAAAQIDBBIRFBUhMVIiQhMyQf/aAAgBAwEBPwFayZeHDq6dfYzs/YhqplR2Lvwzs/Yzs/YSs2+4r2r6EsiIqeOJe3oRvRUXxL29D5G9BI48OC8lfuheNlwLy8R+xp0QtBG7k06ISgjTg06I06IybD//xAAkEQABAwMEAgMBAAAAAAAAAAAAAQIDERRRBEFSoRIxISIjcf/aAAgBAgEBPwG2i4jdJE3YtouJqIY2qyibltFxLaLiO0snktGlZ8dkSTUWiblZ8dkqTKrapuVnx2fvjsuJShAnwv8ASg6PypUoUHJ9lL2bI3VSt9F7NkdqpXey9myXs2RZXqf/xAArEAABAgMHAwMFAAAAAAAAAAABAAIRMTIDBCAhImFxEBJRMEKBEyNBcpH/2gAIAQEABj8COt0wIKp6ZBzjGrZVPVT1U9VPVie5+txBVT1U9VPVT1ZvDnxcSOmeK6/u7CxjTLPNR1DYFDtJarsCdHe4QUIHmKNXMUYuchpP9V2Z7PqOWRIHhe4/KhA5fmKNUfMV9y1I+et32LsN32ccNn5ietjycNnths+T1seT6TOSpKSs8qVJSUlJMypUlJSUk1oGYPrf/8QAJhAAAgICAgEDBAMAAAAAAAAAAREAITFRQfFxIDCBEEBhkbHB0f/aAAgBAQABPyHwK0DLf8KdmIEmOAx2YnZidmJ2YggPuA4nZidmJ2YnZiGTCRmq+lhoBx7fDeQDJBEzqLADD/ghxxaxl/uOUJcFQaZ5jPzny+PE5EKoou2NI0FCISF4OfmIgaaB4hQY+wuCk0aNRMALbDBktB/0ligmiUiGohqMtzvMQ1ENRDUQjLM+IaiF1mIaiGoRGsrNa9gMA5ACwZJrD+1CoMAFxoSGYk/PpPJzMSRbNLhV6SebtIJ+YzuM7jO4zuM7jO4zuM7jO4zuM7jO4zuf/9oADAMBAAIAAwAAABBp/wBjtdDfu69dcyPnPNPPNNuPXPL8/8QAIREAAgICAgEFAAAAAAAAAAAAAAERIWGRMXFBUYGhsdH/2gAIAQMBAT8QTy1Ql55yhqmmXRlj0Y4ZoyzLGQm0ISOl5Ko0Xprg4KbFykVYvHAppjsbOV+PwaRx4REX7CIluPNESImF+F9HZsdJs6OzZMpnZ2bOzYkKJZ//xAAhEQABAwUAAwEBAAAAAAAAAAABABEhMUFhkeEQcbFRgf/aAAgBAgEBPxAALEC+EMLucvPz0sJDAYcQfUrCWErEHLUo8IBu0QjlVcBZWiIBoBpFZULO0QI3aIhJBF/3x2rxA5YX85/6H6sLXUPIISXp1YWupmJCC9OrC11YWuosSSJx1f/EACUQAAIBAwQCAgMBAAAAAAAAAAERACExYVFxkfBB0bHBIKHxgf/aAAgBAQABPxBKuKlStQ/ZuLStWt0tB6Vy9QaoFhO1Q4SPsf5D2j4h7R8TtH1CDgGKAGSpedo+p2j6naPqdo+oGGSiIAkFSUYP7j06UCCzkn6EFWqq+PwF51GfxvMhRCrANWoYWNJqVbHYbQ1DhQEbhahvCV7gAo6BXImBsREFyFT8ISMk4vCF3AFGNPMFK/olDYULre0Ae1yNVh4KD8EwkiEJABh/v4wBrWpqPqCFCSNAWKqRkygTwJF3tmFCggRgJCRkWbiBLCpnGgFjuJTFFKiOJYFll+Yy0BJkTSVAUYiggCbWZfpRvGXD8V0gCwgRq7dMTG1nyDMqNHZwASClsRngQuTcoAJQAxN8E9/8mb5vm+b4TcNWhqeXWb5vm+b55T+sXWLrB3f7mLrF1i6xdYuo5i6xdYusXWDu/wBZT9zptKtvJ9RHwlpJRBFKkp+502mDyfU7ifUpv5nTaBrVQieAN3zO4n1Kb+Z0ek6SfUD/AGPqXYajtUPEyOZkczI5mRzMjmZHMyOZkczI5mRzMjmZHMyOZ//Z",
                    "fallback": require("../images/projects/flappy-fish/screenshot1.jpg"),
                    "webp": require("../images/projects/flappy-fish/screenshot1.webp"),
                    "alt": "Flappy Fish Screenshot",
                }
            },
            {
                year: 2017,
                title: "Global Game Jam 2017 - Pop Up",
                description: `
                    <p>
                        Pop Up is a point and click adventure in which you experience the ripple effect between parallel worlds. Try to manage the hassle of living in the woods by interacting with your paper cutout surroundings. But the careful... something might happen in the other world without you knowing. Controls: Use the mouse to interact and spacebar to flip worlds.
                    </p>
                `,
                duration: '48 hours',
                teamSize: '3',
                labels: [
                    {
                        label: "Unity",
                        category: "engine",
                    },
                    {
                        label: "Programming",
                        category: "role"
                    },
                    {
                        label: "Design",
                        category: "role"
                    },
                    {
                        label: "Pc",
                        category: "platform"
                    },
                    {
                        label: "Gamejam",
                        category: "etc"
                    }
                ],
                accomplishments: [
                    "Rapid prototyping",
                    "Designing and programming interactions"
                ],
                readMore: false,
                download: {
                    platform: "globalgamejam.org",
                    link: "http://globalgamejam.org/2017/games/pop"
                },
                image: {
                    "placeholder": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wgARCAA3AE0DASIAAhEBAxEB/8QAGQABAAMBAQAAAAAAAAAAAAAAAAIEBQMG/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAIB/9oADAMBAAIQAxAAAAH1BXqLAACUwqZ5etYugWmPeLCEG8qka6oQnwlR0sW0iPFyPYYVHsbGJ6jy6+rlYRYCQAAAP//EACEQAAICAwACAgMAAAAAAAAAAAIDAAEEERITIBQiISQw/9oACAEBAAEFApqa9NTXoNF366jHLACyLK0n67g26zY5kKu5rqD+D8o8E4u1nsIXNRjfvN/e9bxcsvCs/KnIcVpTkMAMh3B5Di+StJGeUbFZjr1Gu5PHDieVlBootRBGdHBr9lmakwyq7ySorNXHH8v/xAAUEQEAAAAAAAAAAAAAAAAAAABA/9oACAEDAQE/ASf/xAAUEQEAAAAAAAAAAAAAAAAAAABA/9oACAECAQE/ASf/xAAqEAABAgQEBQQDAAAAAAAAAAACABIBESIxAyEyYRAgI0FCEzBRcaGisf/aAAgBAQAGPwL26sx37c7t5KYhkmlzaBajw34btlUplf7VKcXB3BxL44N8lVfdG50S7R2snYn6FNdOgpoCfOW9/wAfawW6Y3QV02lJU6YUjCEoQXolp8sspKnIbxpTWWXUCf8AU0ey0FwqdlugxidlGBJpBj3+EeIIFETj5J20qZ5LqwjE/b//xAAoEAABAgIIBwEAAAAAAAAAAAABAPAhYRExQVGBkbHRECAwcaHh8cH/2gAIAQEAAT8h6ftNRlOjt55rU78kY85DbpKBK1VtX7hwemgNc+YKEKxYLnxiNi9Fi3TWMmvAkbrmi/FfBDQBmAT7Uibb41fLpr6o1WHYZ1L2qnJ3Q7XqQ1aXmFqwejhWa4EMFUogzdNkityflOjbSSbifaYthbmmME2WDZZGacl+vaUAtFM1KCv8T0//2gAMAwEAAgADAAAAEAwwz/0+zsPFXaXPffffff/EABQRAQAAAAAAAAAAAAAAAAAAAED/2gAIAQMBAT8QJ//EABQRAQAAAAAAAAAAAAAAAAAAAED/2gAIAQIBAT8QJ//EACkQAAEBBAkEAwAAAAAAAAAAAPAAARAhMREwQVFhgbHB0SBxkaFA4fH/2gAIAQEAAT8Q+F6oIlUEVFgJVWVrKIiYoTDWadbGH5gZsWPZ2UU5XhvSiTBarTUak+0W3C5HbA+UYqED4ByXswUFAc46BuhwjoO9jayIu79tc269G4JcDnAaQjUgjm8gpGSTHKWxIQYYxRgZuP4565m9NH2zSdNX/9k=",
                    "fallback": require("../images/projects/pop-up/screenshot1.jpg"),
                    "webp": require("../images/projects/pop-up/screenshot1.webp"),
                    "alt": "Pop Up Screenshot"
                }
            },
            {
                year: 2016,
                title: "Summer Game Dev 2016 - Football Party",
                description: `
                    <p>
                        The Summer Game Dev is a five-day game jam organized by the Dutch Game Garden. This year the theme was to create something for FC United.
                        An event for soccer lovers between six and fourteen years old.
                    </p>
                    <p>
                        We created a fun but simple physics based soccer game for up to eight players. Players can pick up a controller and jump in at any time.
                        Movement is controlled with one of the thumbsticks and by holding one of the buttons players can charge up a dash to make things more dynamic.
                    </p>
                `,
                labels: [
                    {
                        label: "Unity",
                        category: "engine"
                    },
                    {
                        label: "Programming",
                        category: "role"
                    },
                    {
                        label: "Design",
                        category: "role"
                    },
                    {
                        label: "Pc",
                        category: "platform"
                    },
                    {
                        label: "Gamejam",
                        category: "etc"
                    }
                ],
                duration: '5 days',
                teamSize: '5',
                accomplishments: [
                    "Rapid prototyping"
                ],
                readMore: false,
                download: false,
                image: {
                    "placeholder": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wgARCAA2AGADASIAAhEBAxEB/8QAGgAAAgMBAQAAAAAAAAAAAAAAAAMCBQYEAf/EABkBAQADAQEAAAAAAAAAAAAAAAABAgQDBf/aAAwDAQACEAMQAAABrCegM+3RyM1y61pjerULM2rVSMgXtaXHPyWBYVk6/Pws0xPPjoPeBPFa16tPoFrz5/0c3Xo8foOVpCn5OSzydKtUQsmEJuihtqfXbc+B0K6wgiYAwJQ8CEUAf//EACAQAAIDAQADAQADAAAAAAAAAAIDAAEEExESFBAhIiT/2gAIAQEAAQUC/BornFk4sj8THCrGxR8XTi6EBB+rAjMcq1Tz6h6LOfF5AcwocwEvAsViaEE2MQakjSWzQklGNLxpT7PMfYAZsc/STWVOrLjDZRixlmzUynfSTzdfzHncJxP+nSPQJs/oGWvEZ/BjfiPvwRe1zckji6K4Xq0B6IPHoG4XbiRjYMaup1XOq5ocuz6rnZcc3ybj5AV+5rUV6V+1T3JUYRnBe6gW7VB06Di2OrT30Rj9FgxPWEBVLGgS4RKiSogtNXVZ/A8QKgyeZ8YWIIronMAXecaIkK+f/8QAHhEAAgIBBQEAAAAAAAAAAAAAAAIDEhMBECAwQlH/2gAIAQMBAT8BL8LiJ6Jns1d4KyQa/VHEKGAoYej/xAAeEQACAQQDAQAAAAAAAAAAAAAAAgEQERITFCJCA//aAAgBAgEBPwEwOp1pgO/k2YqzU9COIfRDQcU4poEHiMi0Fi0FoLH/xAAyEAABAgQDBQYGAwEAAAAAAAACAAEDERIhIjEyE0FRYZIjQnGBgvAEEGKRsdEUM+FS/9oACAEBAAY/AvlSN1oLpWgulSIInSjcQiYvpWiJ0rRE6ViAm+dI6lSWOIj0whGTvJHEHtRB8WfPL3+Vo7Se/h+1UUHs5ePl+UezgiEvSipgztz1KkQoJpTrtfh9lOvBlwn5LEA+iy4i+kuKqL+xYjxGz+lAUQ8mlUJav9VImTBwG9lrJvwtcTqVNZZN3lijF91SS7Q/KqTSlfwWsTF72yVMTHDfctpHpaH9ZKogntHaUnvPObWWzG5AyMogcmw/f5YtLrxAfwyw2QEOMpeL+/2sVlsxCH2mkpSp5LZxNSAfiTLgKqGkCtlh923Lad185911TWK1j1LWP3Wsch73Jax6lrHqQDApc4mXJUjTgnfOsv0qq5+lbPMmf26pIywBUwjzmqodQFXpH37msUaM4f8ABbn3I6viYlvf6WKNHRiMaNU2HNtSMdsTHxte91V/JjU+81TtohzdsXdVI2HurEBY24KlpiHAUTyk8TVLfJXHfu/1O7vN7Ne6EGezZIn452QSinVz8EYNpnks5kQoWbNsuCmzlOU/N2zTjTZt+9f/xAAnEAABAwIDCQEBAAAAAAAAAAABABEhMVFBYfBxgZGhscHR4fEQIP/aAAgBAQABPyH9V/aL7RavLcuHF/IBffL77+RkS0KbFugRxID5NjZcyRGeRUJZuD0Tyr8RQ7PwhYoL0UGXyO69BZ3LZJkR8dRjbdbcJUhpjADM91pncgFDdV5WPCn4qnS1kMl8CP4N+cLsV8cbmNNriF3SnDk3g9F6Ep7KXtSgBXVSLQ3Kuqr1FIs53BRfEjnve+LrSp9CtERSxO5aK/RZ0aksFrM+sFsd88zOkfynXifupL5Jc/he5PsCTtA8mlyVoFh5orSa1WYLhs4ZlyAHixwDkovIina3Cz6Eb+m79MDkrln6am4g3bJaAMdi5EjQPXq6ifDNq3D8GtokJEh6Xc1XsoAfGZ+IRZ9PMxyXRT+VYlsaV8O1UKoldyZU23wWlFwLCO+JWpptBtu8bO6mVz5Lu2YGbvLzFKS6sMexBz9oVsWL7lRkndt6kkJHFaarTfFGACzYUYvUbczUoJOBGC188NOU4kgHhmfKcOQUs4og6hT2nmSAXq7IIg+SeH4TbhkEMkSMIcDrtZEgcCORcBFaKRzYNrZqq//aAAwDAQACAAMAAAAQwUg0k4MuTM/8i9P30S8Ccgec/8QAHBEAAQMFAAAAAAAAAAAAAAAAARARMDFBUXHR/9oACAEDAQE/EIvas2psY4qVobB//8QAGxEAAgIDAQAAAAAAAAAAAAAAABEgYQEQQaH/2gAIAQIBAT8Q1ZcPeEGOHuYt5pUKVFQp/8QAKBAAAgEDAgUEAwEAAAAAAAAAARHwACExQWEQUXGRoSCBwfEwseHR/9oACAEBAAE/EPSpUUExZGDRbDS/m/wCiq6wRZM9/fo6TQXplnKgnLA2w4KnWf8ARTkHSozb+xo8ibp3bP8ADzmxrGKF3MqgBcApeHFZf5GM3LepWKMcGAS02OjWiSSz1pqGwFhNLj4ttqC9B/enK33YYh5pISX5nU8dYREMEjiTqqI2AeTjQSbkBU8/nBh/wIWXJf1LlwawRFX9w4A6UeI/0ISLVG6tur3DWiLEJY9bthDiCaFSL6fwTZU0p76nvcClJJPw761GqZ8MHsqZ3nkdmSi4qZDKXd8Gii1skagFQZp23arCTSW8x6lIe8JGgtRIiWxbEw55KBoI0fbqgJ7AVQEdp0UbpchMvIA7KZJfdlpjert9XXlNxHvBRspkzQYNnPOMxg5rnIE3cpGFiwKQ3XBZZeoLE3eEgAJsAmwKwWXSBli5FoWN1gLLwhDZK2aJBXuCA8AhJBD03DtWApoAuCCCAYADpmNyp2LtdvJII3BEYEhtEHYIIciLKsKh2KwUidgWSRgkSQSSa//Z",
                    "fallback": require("../images/projects/football-party/screenshot1.jpg"),
                    "webp": require("../images/projects/football-party/screenshot1.webp"),
                    "alt": "Football Party Screenshot"
                }
            },
            {
                year: 2016,
                title: "Unity + Arduino",
                description: `
                    <p>
                        Controlling a unity game with an Arduino connected to a PC.
                    </p>
                    <p>
                        With the increase of VR comes the increase of games with 'more immersive' gameplay. One of the things that can make gameplay immersive are the controls and how closely they match the gameplay on the screen.
                    </p>
                    <p>
                       I set out to figure out if it's possible to build my own controller by using an Arduino with a bunch of connected buttons and switches.  
                    </p>
                `,
                labels: [
                    {
                        label: "Unity",
                        category: "engine"
                    },
                    {
                        label: "Programming",
                        category: "role"
                    },
                    {
                        label: "Design",
                        category: "role"
                    },
                    {
                        label: "Pc",
                        category: "platform"
                    },
                    {
                        label: "Arduino",
                        category: "etc"
                    },
                    {
                        label: "Prototype",
                        category: "etc"
                    }
                ],
                duration: false,
                teamSize: false,
                accomplishments: [
                    "Creating a 'protocol' for the Arduino and Unity to communicate through the serial port",
                    "Setting up the structure to allow connecting additional switches to the Arduino with little effort"
                ],
                readMore: false,
                download: false,
                source: {
                    platform: "Github",
                    link: "https://github.com/Boydbueno/co-operate"
                },
                image: {
                    "placeholder": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wgARCAA2AGADASIAAhEBAxEB/8QAGwAAAQUBAQAAAAAAAAAAAAAABgACAwQHBQH/xAAXAQEBAQEAAAAAAAAAAAAAAAACAAED/9oADAMBAAIQAxAAAAEElRcEIvKEYcvdpB0qhDU2HIiLlKqLoc1CwRj3TZbfGoitNlz8zj0K77CEIuV1FZt7o2eW3SvNJij1ATEdA6uWorWHZbKjpsWdOomF7HPlRmS5L1iW0UqVNcki96SvYkjf/8QAIRAAAQQCAgMBAQAAAAAAAAAAAAECAwQFERIUEBMVNCH/2gAIAQEAAQUCNnM2NUap7WkitHG/DiFnOZ2KafKG4g+UfNkQ6Ew7GzDsZMhYidANozKyRHIY9N3L1uSKy7IWCrekW43ig0cmzrtQ1oyns541bHum5e7EpzuZT+TTcuEjHdmnUmR9GJ0VZr28/EzGvZDE1hY/Thf0zVYZXuxtVRtGFhYhm5wpIkJvw7kU5rT7NxNWWm9HukQ7Ex27B3bA3IWkPpWj6VobkrSn0LBJLze3xpDSGkOLTSGkNIIiJ4cf/8QAGREAAgMBAAAAAAAAAAAAAAAAAAIQEiAw/9oACAEDAQE/AYuXwg8p1//EABgRAAIDAAAAAAAAAAAAAAAAABIwAAIQ/9oACAECAQE/AcCAi7f/xAA2EAAAAwMHCQYHAAAAAAAAAAAAAgMBEiIQETEyM1GiEyBBQmFyktHhBBQjYpGhIUNScYGCk//aAAgBAQAGPwLNiDps4hfraxghWwi2wdRbYOotsIhOm0VEPXoPl8XQavEHVIP2YHikh32A5dZgR+4yZTpsLkn4tLbhCfs1R7pSCIqZM8+sm2gOlleKdT+puch3brqQQvZjxN0BZ6s+2cE2MaD2dlNHTpoCzvdoJqm271B0TOsN5KBlEzleZ5wRNR17YHda6V00i2+0H3OQeUIVphY+48Mkxvy0EMhymDqh51A9LCDpqI+HfdzC2+2SEV1OIWynELZTjFspxC2Ft7CvhYK+FgtsLA8as3OozPhL/8QAJhAAAQEFCAMBAAAAAAAAAAAAAPABEBEgYSExUYGh0eHxQbHBcf/aAAgBAQABPyGarQ2zpGL2s/CJ7G5/Y7IOzBKfBAholM/rllf3Mie0jKxMw8CbL8yzVhVZqhJKaLmbnzO1WCYjfD64Yr2sgZbZzObwIURIow0EpioLSKh5mSm8QHqUSKbiLquCa5O3E9GYmj9xmU5KebgmMjx+ajtx357npsCewI56F9iQlUCgUHagUCgXBg9//9oADAMBAAIAAwAAABDv+7C7oxjzH1DjTBTYKPyByCD/xAAUEQEAAAAAAAAAAAAAAAAAAABA/9oACAEDAQE/EEe//8QAFBEBAAAAAAAAAAAAAAAAAAAAQP/aAAgBAgEBPxBH/8QAIRAAAgECBgMAAAAAAAAAAAAAAGExEPABITBxwfFAUNH/2gAIAQEAAT8Q8nqT1kxVCbzkSp5TTwbg/puyXC3ONASGfuyYfFHlGU1i2p8TNLNt99D3QX8u24fSJMw7iCxqHSL5qrPS95NfUiFgd2eWm2P2JyLkDQ0NDQ0NDRLm6r//2Q==",
                    "fallback": require("../images/projects/unity-arduino/screenshot1.jpg"),
                    "webp": require("../images/projects/unity-arduino/screenshot1.webp"),
                    "alt": "Arduino Controller Photo"
                }
            },
            {
                year: 2016,
                title: "Re-Embodied",
                description: `
                    <p>
                        Re-Embodied is a "Tactical Action Puzzle" game in which the player is only armed with an airgun. they will have to use this airgun to maneuver through puzzles and defeat enemies in creative ways using their surroundings.
                    </p>
                    <p>
                        During the development of Re-Embodied, I took up the role as both programmer and designer. Aside from the programming and design tasks I took up the link between the programming and design teams. With technical know-how and complete knowledge of the design process, I was able to clearly communicate requested features to the programmers and advise the design team. 
                    </p>
                `,
                labels: [
                    {
                        label: "Unity",
                        category: "engine"
                    },
                    {
                        label: "Programming",
                        category: "role"
                    },
                    {
                        label: "Design",
                        category: "role"
                    },
                    {
                        label: "Pc",
                        category: "platform"
                    }
                ],
                duration: '14 weeks',
                teamSize: '14',
                accomplishments: [
                    "Design and programming of structure for connecting triggers and switchables",
                    "Programming and design of the 'air-ball sockets'",
                    "Programming and design of editor tools",
                    "Level design of level 6 and 7",
                    "General gameplay design"
                ],
                readMore: false,
                download: {
                    platform: "GameJolt",
                    link: "http://gamejolt.com/games/re-embodied/121546"
                },
                image: {
                    "placeholder": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wgARCAAbADADASIAAhEBAxEB/8QAGQAAAgMBAAAAAAAAAAAAAAAAAQQCAwUH/8QAFgEBAQEAAAAAAAAAAAAAAAAAAwEC/9oADAMBAAIQAxAAAAHlLjla4XNxO1ynG1bXySmWRoMCmTMi4//EAB8QAAMBAAEEAwAAAAAAAAAAAAECAwARBBAhIhITMv/aAAgBAQABBQLTTwLePuGkzBmPAl0lHesHlQITmUq2bngeuo8r9OWDYYd1/a+undkUn5Nv/8QAHREAAgIBBQAAAAAAAAAAAAAAAREAAhATISMxYf/aAAgBAwEBPwEWFusABw1e8XHqerH/xAAaEQEAAgMBAAAAAAAAAAAAAAABAgMAEDJB/9oACAECAQE/AbKJ1dmmSgZXcwGPjhr/xAAoEAABAwIEBAcAAAAAAAAAAAABAAIhAxEQEjFBM2GR0SAiMlFxgaH/2gAIAQEABj8CV4HM7LhtP2e64TOru6LtL/mAa2nPVFtWHjZNtJOgCLXizhBBwhXbBCzBmWsPXOqbZgbYWjfxUwAPOJjmU9obSOURek07/CJNp9hbD//EAB8QAQACAgIDAQEAAAAAAAAAAAEAESExQVEQYZFx8P/aAAgBAQABPyGNZYNpR/OtwAW7ty+ECq2BAzt1MtVvUSwLnQ/fXuBrYp2+VMWiaRbcXq2gpH3Catbe5mGVWJLQslNjPg+Ri1gs8+21z+TaCr8EIiUNV4pdWHjHUGxZjenLZ3FyYrwD4a8f/9oADAMBAAIAAwAAABDNWayL3//EAB0RAAIBBAMAAAAAAAAAAAAAAAERABAhkfAxQaH/2gAIAQMBAT8Q565aOCo+AKI1RS7TvPmKf//EAB0RAQABBAMBAAAAAAAAAAAAAAEhABARMUFhcYH/2gAIAQIBAT8QmkLJFg0e0hDIa74flCcFv//EAB8QAQADAAICAwEAAAAAAAAAAAEAESExQWFxUZGh0f/aAAgBAQABPxCCsimEG+e1xxapoaYEBCktXygD6Ah0SoM2GVRF6V4zV+9mlTNA7iMZa3RFsHGN6cwmW8AHskT0ygIMhxso3bKs3qIgygCaQOiJVeIfEYoWKVYdwNtADROE+JhE+1CL6DbXe3ukPbYC0PEF6GYEsK+f2B7G22GZZ/IkbMTSCSCUsBVBusdgLuAbF9zISSzlrsEav4QmAxgBVcAB4ADqPE//2Q==",
                    "fallback": require("../images/projects/re-embodied/screenshot1.jpg"),
                    "webp": require("../images/projects/re-embodied/screenshot1.webp"),
                    "alt": "Re-embodied Screenshot"
                }
            },
            {
                year: 2015,
                title: "Super Space Pain",
                description: `
                <p>
                    With only your jetpack and a couple of fuel canisters, you're tasked to escape from a collapsing spaceship. With damaged life support systems, gravity is nonexistent.
                    Can you make it out before you run out of fuel and become a helpless floating astronaut?
                </p>
                `,
                labels: [
                    {
                        label: "Unity",
                        category: "engine"
                    },
                    {
                        label: "Programming",
                        category: "role"
                    },
                    {
                        label: "Design",
                        category: "role"
                    },
                    {
                        label: "Android",
                        category: "platform"
                    }
                ],
                duration: '6 weeks',
                teamSize: '2',
                accomplishments: [
                    "Movement programming and design",
                    "Impact damage calculation",
                    "Level 1 level design",
                    "Audio implementation"
                ],
                readMore: false,
                download: {
                    platform: "Google Play",
                    link: "https://play.google.com/store/apps/details?id=nl.DigitalMelon.SpacePain"
                },
                image: {
                    "placeholder": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wgARCAA2AGADASIAAhEBAxEB/8QAGwAAAQUBAQAAAAAAAAAAAAAAAAECAwQFBwb/xAAZAQACAwEAAAAAAAAAAAAAAAAAAwECBAX/2gAMAwEAAhADEAAAAeY+ht+uXfUt6edwnRcZ7NxLqK1oKbt6s6KSOskkYHoblG5MdGi5XUwMt1G6W6mcowKwjw1oNOvm2ZaxM04p2tiCd1YC26mRZHsfMaUViBT/AHPlgejKrBUaoAiABaCttzLBWj//xAAkEAABAwMDBQEBAAAAAAAAAAACAQMEAAUREBITFCAhMTIGM//aAAgBAQABBQLkJacMW2vzz6v263TW5Ll+a5beibVBWiAy8uLuLTxtgEyyIdDMCHGGMxGgdPMvc8IkXKLR4rdgT8lqlNm+3UG9MqFyvbTDEuS5KdQc6JqzG5R4UVSLdQfLhZolzSe7e9xPuf0TQfKskFOObq5jrlWuSspXivdDij27aH6zkHSwiJSphUHKdw+0HesodqtWSLIq6wOgkqKpSppjsYTLkYMLJ+f/xAAgEQABAgYDAQAAAAAAAAAAAAABAAIDEBEgITESE1Ei/9oACAEDAQE/AWKqiaFg9XYianKEixnEe2Vk44tZtRR8Bf/EACMRAAIBAQcFAAAAAAAAAAAAAAECAAMFEBEgITFBEiIyUXH/2gAIAQIBAT8BqaawqpLMw+Szx2nDbIfUaz6ZYNwOIoCjAXhm6sw3yttKfkZ//8QALxAAAQMDAgMHAgcAAAAAAAAAAQACEQMhMRASE0FRBCAiMHHB8BQjMmFigaGx8f/aAAgBAQAGPwK91vaRxABN5k59vnNrnZkqswWcDz5pwH+I3wYQ4gPrMJzRieinXN0d9GlUJ5vEwqjDRYyo+Gjxn9kyjT/C0KpWY/wvy0j3TscR1gFFltz0USj3Mr7Eh3rC2dpPCeOfJA9mIqVHY6LfVMlW7pIENjJQ2+iunlhft23gc1/Y0CaDOxxh0OhGOqOkBC3hbaY+fkoiCMbdMLGmdLqW6BBQr20mfIA/lEdEfp69dh/W0FGkanEtMxHkNHUwmOxMlAnn891//8QAJRAAAgICAgEDBQEAAAAAAAAAAREAITFBUWFxEIGhIJGxwdHw/9oACAEBAAE/IWL+6C4FjLqwKVqnHxF1tj7/ADBtX4BYIXCixNgsAGDBkASbWRGvkaNV+8cA7NJaOAPj1AeSXEKUQy2icXUBYyAY0Qg0PH8jPaHnuAzkFgX/AJvvmAqwf6YaoBVOEoAmm25cHTj2hNd/QWQMJZHyB9qKgDTZVzBMbzFByZ0Z44hnzQn5h30kYbJ9EBAEl2dqKIEttgEjYh+fV7iVpGgVwvx/t8sh2DwB+pke3cqbTjQAEQRgYya3uoeU7Xz9ph4egEIGTUEAFBJbs37uKaOqLL8zBYHgL8SpJ3CBAs93AbqMhGFED9wQxcEDJePRbMOBMCqSU7ABl5MCQy5Myo3uCYDoc/Q1GefQXeFDVYdN4uF1hLzFaqWA+DLlQAuV7xvYQFiGyVCEYG+jFFFg9iJgkwCc6Oq7j54n+gY//9oADAMBAAIAAwAAABCaRgbSy5tQukIe/wDO9+g8cc/c/8QAHhEBAAIDAAIDAAAAAAAAAAAAAQARITHwEEEgwdH/2gAIAQMBAT8QRSe2VRO1+xWDv664Wx8WMHUMGzMSxTKbli53qVKlQoVUNxBfEWSAIHYn/8QAIREBAAIBAwQDAAAAAAAAAAAAAQARIRAxkSBBYbHB0fD/2gAIAQIBAT8QuBODeUThfbJxEN1fHzKDOtF8ojF4H3BA0EXS2O0tly4mbjAMIV0KlEpP7ef/xAAlEAEBAAICAQMEAwEAAAAAAAABEQAhMUFREGGRcYGhsSDR4fD/2gAIAQEAAT8QIwG1Gz9zAIx3YCkAW7GgxrA1wunvZf2d4rxPfFSB3BZ/WscbF2Bdu015/wCQxWWog6oJx3PfBitEFq00U0cl0YFjNGjprRvXePrONCaAH4PWkFsHQNbX5Jh6s0dh3ohvXMLxicxBJsOAoDoQBHROgCC8oCr3Xb9cvicIBdlWE0VPdozGu0VHSijJdl3NIOIJDlSpee4ZNyAUQPLivzibEm0gnLvwZ7pL+AIWA4Gx1dX7YtMeajE2KXvT4cZlAx4g6Fjzp8fbKIxNG6PBeHR3MdUPEQD0d/K4kOyWwZ9fbrApvQ3x/udtRLZ046vLfQqaXZAcAg7E3dcY08DdiVNqcdHD9hSRALZAcEvsYU68sAu+0GtgBZXWLBhZwBwjzx3jsSoHL9vJ+sFRgO774yMZBlSGgiE2CjrYMdEIgb8goD4PlxAg8JEff0qIcHlc2udCbXCbaPBs1MS0xAA8FMuk4e93FDaGqTfqxfvgfccqq/NPxmujuaHXj/cG2qvjWAHHVjim5pFrd7U3v+vO84YrhFNfjK4E0bhr+78ehWtot4mAtRJa6I984CoJNFKDv7X5wyEIgKTYcb2j8ZSaI0PCaTJgCpsrIoM1p+f4C4KRunFuU+gkKEX8YQHEoCK0HiF92dpgOpRojVin0DnyTWSoUAMK6gd7507y4b181sCuu/K6wQm1UYkOPqIPF3gFTaOic4iD1iCkxIz1iKG1oXdMExCSlmp4NHBdzDTiaNabOuOnHG+c/9k=",
                    "fallback": require("../images/projects/super-space-pain/screenshot1.jpg"),
                    "webp": require("../images/projects/super-space-pain/screenshot1.webp"),
                    "alt": "Super Space Pain Screenshot"
                }
            },
            {
                year: 2013,
                title: "Hondenstreken",
                description: `
                <p>
                    this game was created for children with developmental disabilities. 
                    Because of this target audience, it was important that the game was easy to understand and accessible for a wide range of intellectually challenged children. 
                </p>
                <p>
                    We created a game where you have to 'take care' of the dog by performing small tasks like putting food in the dog's bowl and placing a ball for the dog to retrieve. 
                    Because dragging and dropping is impossible to perform for a big part of the target audience we also added the option to replace dragging by simply clicking the object that has to be moved. 
                    In addition to that, we also added an option where simply clicking anywhere on the screen is enough to complete the task.
                </p>
                `,
                accomplishments: [
                    "Set up of structure for creating levels",
                    "Set up logic for switching levels",
                ],
                labels: [
                    {
                        label: "XNA",
                        category: "engine"
                    },
                    {
                        label: "Programming",
                        category: "role"
                    },
                    {
                        label: "Design",
                        category: "role"
                    },
                    {
                        label: "Pc",
                        category: "platform"
                    }
                ],
                duration: '',
                teamSize: '2',
                readMore: false,
                download: false,
                source: {
                    platform: "Github",
                    link: "https://github.com/Boydbueno/Hondenstreken"
                },
                image: {
                    "placeholder": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAAA3CAMAAADEzb9FAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAABaUExURWY2IO7u7mJEK6WSfvDw8HdZN39hP52IaHBUMa17Q3VXNY52VP7+/tC/pbWCS52FZYODg7KxsKZ5RbWnkHI/HphqPGZmZnxOKOHe1c5EL+cfIiYoJEUoAg8IAYnpdowAAAGdSURBVFjD7dfbcoIwFAXQQAjhQG4EKGjb///NJlymVhOIYB7ssB9wBnEvD0FRhM74QwiJWd8TElUgc2L3k9j9UVeBlG3kq6hEJ3ACJ3ACbwYAwPgohIgCmH7QU7+ACEALYzSACB/hGWCsFwCMQ3cEKHyx/Yxp9snAnKX29QDJyQgoA6AuwgTTDADZygrzJfsBbTeu85PmJpw3mEpZ8XzOA1AU60D3mZpt7wLSlDhShtdPE3yDH3C9pgyvL0pYguIAdoKv2MBLJsjdKYp+pf8FQF7oy/V6uVyu7U6g3QDy5QzpncDWIoija3AYqMghoN8EMGP44SvS/v2gc+yOyhv9C1BXzFcFM6nuAUIRDYrcAGhaWOBuAvsEwmGC0OsApRZw7N4C+JBMWfq75E+GRs5HYkZdAsLSUz0kdV0rpZp6DZjDlylCJ7AvqtXHGGUFCs10y3EDJuMbxQ4A+4BEKTNDjdU0QHLzsDJEIDASak4SEO5bRC9gr006ZNlWczY0VI4HPw/cUFJS3DTzr4ba3Ny9hQ4g8JOwM/8DwG8N/ADm70ei8VvUQAAAAABJRU5ErkJggg==",
                    "fallback": require("../images/projects/hondenstreken/screenshot1.png"),
                    "webp": require("../images/projects/hondenstreken/screenshot1.webp"),
                    "alt": "Hondenstreken Screenshot"
                }
            }
        ],

    }
});
