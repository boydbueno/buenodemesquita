let WebFont = require('webfontloader');

WebFont.load({
    google: {
        families: ['Noto Sans']
    }
});
