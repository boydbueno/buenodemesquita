<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#262626">
    <link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQEAYAAABPYyMiAAAABmJLR0T///////8JWPfcAAAACXBIWXMAAABIAAAASABGyWs+AAAAF0lEQVRIx2NgGAWjYBSMglEwCkbBSAcACBAAAeaR9cIAAAAASUVORK5CYII=" rel="icon" type="image/x-icon"/>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <title>Boyd Bueno de Mesquita</title>
    @include('partials.analytics')
</head>
<body>

<div id="app" v-cloak>
    <bb-header></bb-header>

    <main class="container">
        <bb-projects :projects-grouped-by-year="projectsGroupedByYear"></bb-projects>
    </main>

    <bb-footer></bb-footer>

</div>

<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ mix('js/fonts.js') }}" async></script>

</body>
</html>
