const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/fonts.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css').version()
    .webpackConfig({
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.common.js',
                'Components': path.resolve(__dirname, 'resources/assets/js/components/')
            }
        },
        module: {
            rules: [
                {
                    // For this rule to work, the image rule in laravel-mix must be disabled
                    // TODO: Properly override the standard laravel-mix image loader
                    test: /\.(png|jpe?g|gif)$/,
                    loaders: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: path => {
                                    path = path.replace(/\\/g, "/");
                                    let pathParts = path.split("/");

                                    path = pathParts.splice(pathParts.indexOf("images")).join("/");
                                    return path + '?[hash]';
                                },
                                publicPath: '/'
                            }
                        },
                        {
                            loader: 'img-loader',
                            options: Config.imgLoaderOptions
                        }
                    ]
                },
                {
                    test: /\.webp$/,
                    loaders: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: path => {
                                    path = path.replace(/\\/g, "/");
                                    let pathParts = path.split("/");

                                    path =  pathParts.splice(pathParts.indexOf("images")).join("/");
                                    return path + '?[hash]';
                                },
                                publicPath: '/'
                            }
                        }
                    ]
                }
            ]
        }
    });